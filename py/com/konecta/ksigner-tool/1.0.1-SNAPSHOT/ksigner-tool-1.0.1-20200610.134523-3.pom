<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>py.com.konecta</groupId>
    <artifactId>ksigner-tool</artifactId>
    <version>1.0.1-SNAPSHOT</version>
	
    <parent>
        <groupId>net.shibboleth</groupId>
        <artifactId>parent-v3</artifactId>
        <version>9</version>
    </parent>
       
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <opensaml.groupId>org.opensaml</opensaml.groupId>
        <opensaml.version>3.2.0</opensaml.version>
        <java-support.version>7.2.0</java-support.version>
    </properties>

    <dependencies>
        <!-- Compile dependencies -->
        <dependency>
            <groupId>net.shibboleth.utilities</groupId>
            <artifactId>java-support</artifactId>
            <version>${java-support.version}</version>
        </dependency>
        <dependency>
            <groupId>${opensaml.groupId}</groupId>
            <artifactId>opensaml-core</artifactId>
            <version>${opensaml.version}</version>
        </dependency>
        <dependency>
            <groupId>${opensaml.groupId}</groupId>
            <artifactId>opensaml-security-api</artifactId>
            <version>${opensaml.version}</version>
        </dependency>
        <dependency>
            <groupId>${opensaml.groupId}</groupId>
            <artifactId>opensaml-xmlsec-api</artifactId>
            <version>${opensaml.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.santuario</groupId>
            <artifactId>xmlsec</artifactId>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk15on</artifactId>
        </dependency>
        <dependency>
            <groupId>com.beust</groupId>
            <artifactId>jcommander</artifactId>
        </dependency>
        <dependency>
            <groupId>${httpclient.groupId}</groupId>
            <artifactId>httpclient</artifactId>
        </dependency>
        <dependency>
            <groupId>${httpclient.groupId}</groupId>
            <artifactId>httpcore</artifactId>
            <version>4.3.3</version>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcpkix-jdk15on</artifactId>
            <version>1.54</version>
            <type>jar</type>
        </dependency>

        <!-- Runtime dependencies -->
        <dependency>
            <groupId>${opensaml.groupId}</groupId>
            <artifactId>opensaml-xmlsec-impl</artifactId>
            <version>${opensaml.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>${slf4j.groupId}</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <scope>runtime</scope>
        </dependency>
   

        <!-- Test Dependencies -->
        <dependency>
            <groupId>${opensaml.groupId}</groupId>
            <artifactId>opensaml-security-api</artifactId>
            <version>${opensaml.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        
        

    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>crm-snapshots</id>
            <url>http://asusisv-git.sis.personal.net.py/nexus/content/repositories/crm-recambio-snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>crm-releases</id>
            <url>http://asusisv-git.sis.personal.net.py/nexus/content/repositories/crm-recambio-releases/</url>
        </repository>
    </distributionManagement>
	
    <repositories>
        <repository>
            <id>crm-public-repository</id>
            <url>http://asusisv-git.sis.personal.net.py/nexus/content/groups/crm-repositories-group/</url>
        </repository>
        <repository>
            <id>shib-release</id>
            <url>https://shibboleth.net/nexus/content/groups/public</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>shib-snapshot</id>
            <url>https://shibboleth.net/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>

    
    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-assembly-plugin</artifactId>
                        <configuration>
                            <descriptors>
                                <descriptor>src/main/assembly/bin.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>